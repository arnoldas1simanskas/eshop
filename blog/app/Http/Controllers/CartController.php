<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //session()->forget('cart');

        $products = User::cartItems();
        $cartTotal = User::cartValue();

        if(empty($products)){
            session()->flash('message', 'Your cart is empty');
            session()->flash('message-class', 'alert-danger');
        }

        //dd($products);
        return view('cart.index', [
            'products' => $products,
            'cartTotal' => $cartTotal,
        ]);
    }
    public function checkout(){

        return view('cart.checkout');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::findOrFail(request('product_id'));

        $cart = session()->get('cart');

        if($cart == null){
            $cart = [];
        }


        if(array_key_exists($product->id, $cart)){
            $cart[$product->id]['quantity']++;
        } else {
            $cart[$product->id] = [
                'id' => $product->id,
                'title' => $product->title,
                'image' => $product->image,
                'price' => $product->price,
                'quantity' => 1
            ];
        }

        session()->put('cart', $cart);

        session()->flash('message', 'Your item was added');
        session()->flash('message-class', 'alert-success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cart = session()->get('cart');
        if($request->has('number')){
            $cart[$id]['quantity'] = request('number');
        }
        //dd($cart);
        session()->put('cart', $cart);

        session()->flash('message', 'Successfully updated quantity of your item');
        session()->flash('message-class', 'alert-success');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart = session()->get('cart');
        unset($cart[$id]);
        session()->put('cart', $cart);

        session()->flash('message', 'Your item was removed');
        session()->flash('message-class', 'alert-danger');

        return redirect()->back();
    }
}
