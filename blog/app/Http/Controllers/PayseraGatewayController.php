<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class PayseraGatewayController extends Controller
{
    //
    public function redirect($id){
        $order = Order::find($id);
        // Paimame payseros nustatymus iš config/services.php failo
        $config = config('services.paysera');
        // Nustatome pagal nuožiūrą
        $orderId = $order->id;
        $params = [
            'projectid' => $config['projectid'],
            'orderid' => $orderId,
            'accepturl' => $config['accepturl'],
            'cancelurl' => $config['cancelurl'],
            'callbackurl' => $config['callbackurl'],
            'version' => $config['version'],
            'test' => $config['test'],
            'p_email' => $order->email,
            'amount' => intval($order->getOrderTotal() * 100)
        ];

        // Užkoduojame parametrus ir paruošiame parašą.
        $params = http_build_query($params);
        $params = base64_encode($params);
        $data = str_replace('/', '_', str_replace('+', '-', $params));
        $sign = md5($data . $config['password']);

        // Nukreipiame vartotoją į Payseros puslapį.
        return redirect('https://www.paysera.com/pay/' . '?data=' . $data . '&sign=' . $sign);
    }

    public function callback (Request $request)
    {
        $sign = $config = config('services.paysera.password');
        $data = $request->data;
        $ss1 = $request->ss1;
        // Sutikriname parašus
        if (md5($data . $sign) === $ss1) {
        // Iškoduojame parametrus
            $params = array();
            parse_str(base64_decode(strtr($request->data, array('-' => '+', '_' => '/'))), $params);
        // Naudojame pvz.: $p_email, $name, $surname kintamuosius užsakymo patvirtinimui.
        }
        return response('OK', 200);
    }

    public function success(Request $request){
        parse_str(base64_decode(strtr($request->input('data'), array('-' => '+', '_' => '/'))), $params);

        $order = Order::find($params['orderid']);
        $order->status = 'paid';
        $order->save();
    }
}
