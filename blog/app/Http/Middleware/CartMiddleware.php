<?php

namespace App\Http\Middleware;

use App\Product;
use Closure;

class CartMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sessionProducts = session()->get('cart');

        $productsId = [];

        $cartTotal = 0;

        foreach ($sessionProducts as $product){
            $productsId[] = $product['id'];
        }
        $products = Product::whereIn('id', $productsId)->get();

        foreach ($products as $product){
            $product->quantity = $sessionProducts[$product->id]['quantity'];
            $cartTotal += $product->price * $product->quantity;
        }

        if(empty($products)){
            session()->flash('message', 'Your cart is empty');
            session()->flash('message-class', 'alert-danger');
        }

        //dd($products);
        return view('layouts.shop', [
            'products' => $products,
            'cartTotal' => $cartTotal,
        ]);
    }
}
