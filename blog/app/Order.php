<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    public function orderItems(){
        return $this->hasMany('App\OrderProduct', 'order_id', 'id');
    }

    public function getOrderTotal(){
        $amount = 0;
        foreach ($this->orderItems as $item){
            $price = $item->product->price * $item->quantity;

            $amount += $price;
        }

        return $amount;
    }
}
