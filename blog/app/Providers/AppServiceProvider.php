<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) use (&$auth) {
            //$cart = Session::get('cart');
            $cart = User::cartItems();
            $cartValue = User::cartValue();
            $view->with([
                'cart' => $cart,
                'cartValue' =>$cartValue
            ]);
        });
        Schema::defaultStringLength(191);
    }
}
