<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function cartItems() {
        $sessionProducts = session()->get('cart');

        $productsId = [];

        $cartTotal = 0;

        foreach ($sessionProducts as $product){
            $productsId[] = $product['id'];
        }
        $products = Product::whereIn('id', $productsId)->get();

        foreach ($products as $product){
            $product->quantity = $sessionProducts[$product->id]['quantity'];
            $cartTotal += $product->price * $product->quantity;
        }

        return $products;
    }

    public static function cartValue(){
        $sessionProducts = session()->get('cart');

        $productsId = [];

        $cartTotal = 0;

        foreach ($sessionProducts as $product){
            $productsId[] = $product['id'];
        }
        $products = Product::whereIn('id', $productsId)->get();

        foreach ($products as $product){
            $product->quantity = $sessionProducts[$product->id]['quantity'];
            $cartTotal += $product->price * $product->quantity;
        }

        return $cartTotal;
    }
}
