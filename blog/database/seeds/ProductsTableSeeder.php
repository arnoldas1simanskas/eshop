<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i =0; $i < 10; $i++){
            $product = new Product();
            $product->title = $faker->sentence(3);
            $product->price = $faker->randomFloat(2);
            $product->image = $faker->imageUrl(640, 480, 'technics');
            $product->description = $faker->paragraphs(3, true);

            $product->save();
        }

    }
}
