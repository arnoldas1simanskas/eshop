@extends('layouts.shop')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <form method="post" action="{{ route('order.store') }}">
                    @csrf
                    <label>Name</label>
                    <input class="form-control" type="text" name="full_name">
                    <label>email</label>
                    <input class="form-control" type="text" name="email">
                    <label> address</label>
                    <input class="form-control" type="text" name="delivery_address">
                    <input type="submit">
                </form>
            </div>
        </div>
    </div>
@endsection