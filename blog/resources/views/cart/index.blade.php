@extends('layouts.shop')
@section('content')
    <!-- cart-main-area start -->
    @if(isset($products))
    <div class="cart-main-area ptb--100 bg__white">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
{{--                    <form method="post" action="#">--}}
                        <div class="table-content table-responsive">
                            <table>
                                <thead>
                                <tr>
                                    <th class="product-thumbnail">products</th>
                                    <th class="product-name">name of products</th>
                                    <th class="product-price">Price</th>
                                    <th class="product-quantity">Quantity</th>
                                    <th class="product-subtotal">Total</th>
                                    <th class="product-remove">Remove</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($products as $product)
                                    <tr>
                                        <td class="product-thumbnail">
                                            <a href="{{route('products.show', [$product->id])}}">
                                                <img src="{{ $product->image }}" alt="product img" />
                                            </a>
                                        </td>
                                        <td class="product-name"><a href="{{route('products.show', [$product->id])}}">{{$product->title}}</a>
                                            <ul  class="pro__prize">
                                                <li>${{$product->price}}</li>
                                            </ul>
                                        </td>
                                        <td class="product-price"><span class="amount">{{ $product->price * $product->quantity }}</span></td>
                                        <td class="product-quantity">

                                            <form method="post" action="{{route('cart.update', [$product->id])}}">
                                                @csrf
                                                @method('put')
                                                <input type="number" name="number" value="{{ $product->quantity }}" />
                                                <button class="action" type="submit">Up</button>
                                            </form>

                                        </td>
                                        <td class="product-subtotal">${{ $product->price * $product->quantity }}</td>
                                        <td class="product-remove">
                                            <form method="post" action="{{ route('cart.destroy', [$product->id]) }}">
                                                @csrf
                                                @method('delete')
                                                <button onclick="$(this).closest('form').submit()" class="action" type="submit" >
                                                    <i class="icon-trash icons"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="buttons-cart--inner">
                                    <div class="buttons-cart">
                                        <a href="{{ route('products.index') }}">Continue Shopping</a>
                                    </div>
                                    <div class="buttons-cart checkout--btn">

                                        <a href="#">update</a>
                                        <a href="{{ route('cart.checkout') }}">checkout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="ht__coupon__code">
                                    <span>enter your discount code</span>
                                    <div class="coupon__box">
                                        <input type="text" placeholder="">
                                        <div class="ht__cp__btn">
                                            <a href="#">enter</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 smt-40 xmt-40">
                                <div class="htc__cart__total">
                                    <h6>cart total</h6>
                                    <div class="cart__desk__list">
                                        <ul class="cart__desc">
                                            <li>cart total</li>
                                            <li>shipping</li>
                                        </ul>
                                        <ul class="cart__price">
                                            <li>${{ $cartTotal }}</li>
                                            <li>0</li>
                                        </ul>
                                    </div>
                                    <div class="cart__total">
                                        <span>order total</span>
                                        <span>${{ $cartTotal }}</span>
                                    </div>
                                    <ul class="payment__btn">
                                        <li class="active"><a href="#">payment</a></li>
                                        <li><a href="{{ route('products.index') }}">continue shopping</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
{{--                    </form>--}}
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- cart-main-area end -->
@endsection