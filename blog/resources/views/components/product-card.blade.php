<div class="col-md-4 col-lg-3 col-sm-6 col-xs-12 single-product">
    <div class="category">
        <div class="ht__cat__thumb">
            <a href="{{ route('products.show', [$product->id]) }}">
                <img src="{{ $product->image }}" alt="product images">
            </a>
        </div>
        <div class="fr__hover__info">
            <ul class="product__action">
                <li><a href="wishlist.html"><i class="icon-heart icons"></i></a></li>

                <li>
                    <form method="post" action="{{ route('cart.store') }}">
                        @csrf
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <button class="action" type="submit" >
                            <i class="icon-handbag icons"></i>
                        </button>
                    </form>
                </li>

                <li><a href="#"><i class="icon-shuffle icons"></i></a></li>
            </ul>
        </div>
        <div class="fr__product__inner">
            <h4><a href="product-details.html">{{ $product->title }}</a></h4>
            <ul class="fr__pro__prize">
                <li>${{ $product->price }}</li>
            </ul>
        </div>
    </div>
</div>