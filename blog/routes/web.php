<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('products', 'ProductController');
Route::get('cart/checkout', 'CartController@checkout')->name('cart.checkout');
Route::resource('cart', 'CartController');
Route::resource('order', 'OrderController');

Route::get('/paysera/redirect/{id}', 'PayseraGatewayController@redirect')->name('paysera-redirect');
Route::get('/paysera/callback', 'PayseraGatewayController@callback')->name('paysera-callback');
Route::get('/paysera/uzsakymas-pavyko', 'PayseraGatewayController@success');
Route::get('/paysera/uzsakymas-nepavyko', function () {
    echo 'nepavyko';
});

